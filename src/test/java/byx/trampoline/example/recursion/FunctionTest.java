package byx.trampoline.example.recursion;

import byx.trampoline.core.Trampoline;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static byx.trampoline.core.Trampolines.exec;
import static byx.trampoline.core.Trampolines.value;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FunctionTest {
    @Test
    public void testFunction() {
        Function<Integer, Integer> f = n -> n;
        for (int i = 0; i < 100000; i++) {
            f = f.andThen(n -> n + 1);
        }
        Function<Integer, Integer> t = f;
        assertThrows(StackOverflowError.class, () -> t.apply(0));
    }

    @Test
    public void testSafeFunction() {
        SafeFunction<Integer, Integer> f = n -> n;
        for (int i = 0; i < 100000; i++) {
            f = f.andThen(n -> n + 1);
        }
        assertEquals(100000, f.apply(0));
    }
}

interface SafeFunction<T, R> {
    R doApply0(T t);

    default Trampoline<R> doApply(T t) {
        return value(() -> doApply0(t));
    }

    default R apply(T t) {
        return doApply(t).run();
    }

    default <V> SafeFunction<T, V> andThen(SafeFunction<R, V> after) {
        return new SafeFunction<>() {
            @Override
            public V doApply0(T t) {
                return null;
            }

            @Override
            public Trampoline<V> doApply(T t) {
                return exec(() -> SafeFunction.this.doApply(t))
                    .flatMap(after::doApply);
            }
        };
    }
}
